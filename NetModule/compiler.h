#ifndef _COMPILER_H
#define _COMPILER_H

#define MIN(x,y) (((x)<(y)) ? (x) : (y))

typedef unsigned char BYTE;
typedef unsigned long DWORD;
typedef unsigned short WORD;
typedef unsigned int UINT;

//#define INTERRUPT __attribute__ ((isr, section (".fastrun")))
#define INTERRUPT
#define FASTRUN __attribute__ ((long_call, __noinline__, section (".fastrun")))
//#define FASTRUN
//#define FASTRUN __attribute__ ((section (".fastrun")))

#define hi8(x) ((x)>>8)
#define lo8(x) ((x)&0xff)

#define swap_word(x) ((WORD)(((x)>>8)|((x)<<8)))
#define swap_dword(x) (((x)>>24) | ((x)<<24) | (((x)&0x00ff0000)>>8) | (((x)&0x0000ff00)<<8) )

#define COMPILE_TIME_ASSERT(constant_expr) \
 do { \
 switch(0) { \
 case 0: \
 case constant_expr: \
 ; \
 } \
 } while(0)
 

/*
#define MSG(x) Send_Msg(x,sizeof(x))
void Send_Msg(const char * msg, int len);
void Send_Reg(unsigned long reg, int len);
void Send_Reg_Hex(unsigned long reg, int len);*/

template<typename iterator_type, typename const_iterator_type>
inline iterator_type copy(iterator_type dest, const_iterator_type from, const_iterator_type to)
{
	for(;from!=to;++from)
	{
		*dest=*from;
		++dest;
	}
	return dest;
}

inline void fast_copy(BYTE * dest, const BYTE * from, const BYTE * to)	//TODO: make it fast ;)
{
	copy(dest,from,to);
}

template<typename iter1, typename iter2>
inline int compare_strings(iter1 str1, iter2 str2, char sep=' ', unsigned max_len = 32)
{
	for(;*str1==*str2 && max_len--;++str1,++str2)
	{
		if(*str1 == 0)
			return 0;
	}
	if((*str1==0||*str1==sep) && (*str2==0||*str2==sep))
		return 0;
	else
		return *str1>*str2 ? 1 : -1;
}

///////////////////////////a little checksum thing //////////////////////////
inline unsigned short crc_ccitt_update (unsigned short crc, unsigned char data)
{
	data ^= (crc&0x00ff);
	data ^= data << 4;
	return ((((unsigned short)data << 8) |(crc>>8)) ^ (unsigned char)(data >> 4) ^ ((unsigned short)data << 3));
}

inline unsigned short crcx2 (unsigned short crc, short data)
{
	unsigned short ret = crc_ccitt_update(crc, (data)&0xff);
	return crc_ccitt_update(ret, (data>>8)&0xff);
}

inline unsigned short crcx4 (unsigned short crc, long data)
{
	unsigned short ret = crc_ccitt_update(crc, (data)&0xff);
	ret = crc_ccitt_update(ret, (data>>8)&0xff);
	ret = crc_ccitt_update(ret, (data>>16)&0xff);
	return crc_ccitt_update(ret, (data>>24)&0xff);
}

#endif
