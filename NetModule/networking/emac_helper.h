#ifndef __emac_helper_h
#define __emac_helper_h


#define TX_USED	(1<<31)
#define TX_WRAP	(1<<30)
#define TX_LAST_BUFFER	(1<<15)

#define RX_USED	(1<<0)
#define RX_WRAP	(1<<1)
#define RX_EOF	(1<<15)
#define RX_SOF	(1<<14)


#include "setup.h"
#include "compiler.h"
#include "board.h"
#include <new>
#include <cstddef>

struct data
{
	static const WORD bufsize = 128;
	static data * head;
	static void * operator new(size_t size, const std::nothrow_t& nothrow_constant) throw()
	{
		if(!head)
			return 0;
		data * tmp = head;
		head = head->next;
		return tmp;
	}
	void operator delete(void * location, size_t size)
	{
		if(location==0)
			return;
		static_cast<data*>(location)->next = head;
		head = static_cast<data*>(location);
	}
	static void init(DWORD * raw_data, unsigned count)
	{
		head=0;
		while(count--)
		{
			data * nb = reinterpret_cast<data*>(raw_data);
			nb->next = head;
			head = nb;
			raw_data+=bufsize/sizeof(DWORD);
		}
	}
	static bool is_empty()
	{
		return !head;
	}
	union
	{
		const BYTE * data_ptr;
		data * next;
	};
	WORD dsize;
	WORD hsize;
	BYTE headers[0];
	
	inline data(const BYTE * dptr, WORD datasize = 0, WORD headersize = 0)
		: data_ptr(dptr)
		, dsize(datasize)
		, hsize(headersize)
	{
	}
	inline WORD data_size()
	{
		return dsize;
	}
	inline WORD header_size() const
	{
		return hsize;
	}
	inline WORD size() const
	{
		return dsize+hsize;
	}
	inline const BYTE * get_data() const
	{
		return data_ptr;
	}
	inline const BYTE * get_header() const
	{
		return headers;
	}
}__attribute__((packed));

template<typename T>
struct buf_iterator
{
	const T * bufaddr;
	const T * wrapaddr;
	const T & operator *()
	{
		return *bufaddr;
	}
	const T & operator [](unsigned n)
	{
		const T * paddr = bufaddr+n;
		if(paddr>=wrapaddr)
			paddr -= RECBUF_NUMBER*32;
		return *paddr;
	}
	buf_iterator & operator++ ()
	{
		bufaddr++;
		if(bufaddr==wrapaddr)
			bufaddr -= RECBUF_NUMBER*32;
		return *this;
	}
	buf_iterator operator+(unsigned n)
	{
		const T * paddr = bufaddr+n;
		while(paddr>=wrapaddr)
			paddr -= RECBUF_NUMBER*(128/sizeof(T));
		return buf_iterator(paddr,wrapaddr);
	}
	bool operator==(const buf_iterator & other)
	{
		return bufaddr==other.bufaddr;
	}
	bool operator!=(const buf_iterator & other)
	{
		return bufaddr!=other.bufaddr;
	}
	const void * get_pointer()
	{
		return bufaddr;
	}
	buf_iterator<unsigned char> byte_iterator()
	{
		return buf_iterator<unsigned char>(bufaddr,wrapaddr);
	}
	buf_iterator(const void * start, const void * wrap)
		: bufaddr(static_cast<const T*>(start))
		, wrapaddr(static_cast<const T*>(wrap))
	{
	}
};

typedef buf_iterator<unsigned long> rbuf_iterator;

struct emac_rx_desc
{
	unsigned long address;
	unsigned long status;
}__attribute__((packed, aligned(8)));
struct emac_tx_desc
{
	unsigned long address;
	unsigned long status;
}__attribute__((packed, aligned(8)));
	
struct in_frame;

struct emac_driver
{
	unsigned long rbuffers[RECBUF_NUMBER][32];
	volatile emac_rx_desc rx_list[RECBUF_NUMBER];
	volatile emac_tx_desc tx_list[TXBUF_NUMBER];
	data * frames[TXBUF_NUMBER/2];
	unsigned next_tx_buf;
	unsigned next_rx_buf;
	unsigned next_to_delete;
	
	void init_rx_list()
	{
		for(int i=0;i<RECBUF_NUMBER;i++)
		{
			rx_list[i].address = reinterpret_cast<unsigned long>(rbuffers[i]);
		}
		rx_list[RECBUF_NUMBER-1].address |= RX_WRAP; //wrap bit
	}
	void init_tx_list()
	{
		for(int i=0;i<TXBUF_NUMBER;i++)
		{
			tx_list[i].status = TX_USED;	//used bit
		}
		tx_list[TXBUF_NUMBER-1].status |= TX_WRAP; //wrap bit
	}
	emac_driver()
		: next_tx_buf(0)
		, next_rx_buf(0)
	{
		init_rx_list();
		init_tx_list();
	}
	
	void release_rx_bufs(unsigned from, unsigned to)
	{
		while(from!=to)
		{
			rx_list[from].address &= (~RX_USED);
			from++;
			from &= (RECBUF_NUMBER-1);
		}
	}
	
	rbuf_iterator get_rbuf_iterator(unsigned first_buf) const
	{
		return rbuf_iterator(rbuffers[first_buf], rbuffers[RECBUF_NUMBER]);
	}
	
	void init(BYTE phyaddr, const BYTE * pMacAddress)
	{
		AT91C_BASE_PMC->PMC_PCER = 1 << 16;//id;	// Power ON
		AT91C_BASE_EMAC->EMAC_NCR = 0;			// Disable TX & RX and more 
		AT91C_BASE_EMAC->EMAC_IDR = ~0;			// disable interrupts

//		EMAC_ResetRx();
//		EMAC_ResetTx();

		// Set the MAC address
		if( pMacAddress != (BYTE *)0 ) {
			AT91C_BASE_EMAC->EMAC_SA1L = ( ((unsigned int)pMacAddress[3] << 24)
										 | ((unsigned int)pMacAddress[2] << 16)
										 | ((unsigned int)pMacAddress[1] << 8 )
										 |                pMacAddress[0] );

			AT91C_BASE_EMAC->EMAC_SA1H = ( ((unsigned int)pMacAddress[5] << 8 )
										 |                pMacAddress[4] );
		}

		AT91C_BASE_EMAC->EMAC_NCR = AT91C_EMAC_CLRSTAT;

		AT91C_BASE_EMAC->EMAC_RSR = (AT91C_EMAC_OVR | AT91C_EMAC_REC | AT91C_EMAC_BNA);	// Clear all status bits in the receive status register.

		AT91C_BASE_EMAC->EMAC_TSR = ( AT91C_EMAC_UBR | AT91C_EMAC_COL | AT91C_EMAC_RLES	// Clear all status bits in the transmit status register
									| AT91C_EMAC_BEX | AT91C_EMAC_COMP
									| AT91C_EMAC_UND );

		AT91C_BASE_EMAC->EMAC_ISR;		// Clear interrupts

		AT91C_BASE_EMAC->EMAC_NCFGR |= (AT91C_EMAC_DRFCS | AT91C_EMAC_PAE);	// Enable the copy of data into the buffers, ignore broadcasts, and don't copy FCS.
		AT91C_BASE_EMAC->EMAC_NCFGR |= (2<<14);	//offset received frames by 2 bytes
/*		if( enableCAF == EMAC_CAF_ENABLE ) {
			AT91C_BASE_EMAC->EMAC_NCFGR |= AT91C_EMAC_CAF;
		}
		if( enableNBC == EMAC_NBC_ENABLE ) {
			AT91C_BASE_EMAC->EMAC_NCFGR |= AT91C_EMAC_NBC;
		}*/

		AT91C_BASE_EMAC->EMAC_TBQP = reinterpret_cast<DWORD>(tx_list);	//load circular buffer registers
		AT91C_BASE_EMAC->EMAC_RBQP = reinterpret_cast<DWORD>(rx_list);
		
		AT91C_BASE_EMAC->EMAC_NCR |= (AT91C_EMAC_TE | AT91C_EMAC_RE | AT91C_EMAC_WESTAT);	// Enable Rx and Tx, plus the stats register.

		// Setup the interrupts for TX (and errors)
/*		AT91C_BASE_EMAC->EMAC_IER = AT91C_EMAC_RXUBR
								  | AT91C_EMAC_TUNDR
								  | AT91C_EMAC_RLEX
								  | AT91C_EMAC_TXERR
								  | AT91C_EMAC_TCOMP
								  | AT91C_EMAC_ROVR
								  | AT91C_EMAC_HRESP
								  | AT91C_EMAC_PFRE
								  | AT91C_EMAC_PTZ
								  ;*/
								  
		AT91C_BASE_EMAC->EMAC_USRIO = AT91C_EMAC_CLKEN;	//enable MII
	}
	
	BYTE queue_frame(data * fr, BYTE deleteaftersend = 1)
	{
		if(!fr)
			return 0;
		delete_sent();	//some clean-up
		if(!(tx_list[next_tx_buf].status & TX_USED))
		{
			if(deleteaftersend)
				delete fr;
			return 0;
		}
		delete frames[next_tx_buf>>1];
		unsigned size1 = fr->header_size();
		unsigned size2 = fr->data_size();
		DWORD ad1 = reinterpret_cast<DWORD>(fr->get_header());
		DWORD ad2 = reinterpret_cast<DWORD>(fr->get_data());
		if(size2==0)
		{
			if(size1==0)
				return 0;
			ad2=ad1;
			size2 = size1;
			size1=0;
		}
		frames[next_tx_buf>>1]=deleteaftersend ? fr : 0;
		tx_list[next_tx_buf].address = ad1;
		tx_list[next_tx_buf].status = (size1 & 2047) ;
		next_tx_buf++;
		tx_list[next_tx_buf].address = ad2;
		tx_list[next_tx_buf].status = (size2 & 2047) | TX_LAST_BUFFER | (next_tx_buf == (TXBUF_NUMBER-1) ? TX_WRAP : 0);
		next_tx_buf++;
		next_tx_buf &= (TXBUF_NUMBER-1);
		// Now start to transmit if it is not already done
		AT91C_BASE_EMAC->EMAC_NCR |= AT91C_EMAC_TSTART;
		return 1;
	}
	
	void delete_sent()
	{
		if(next_to_delete!=next_tx_buf && (tx_list[next_to_delete].status & TX_USED))
		{
			delete frames[next_to_delete>>1];
			frames[next_to_delete>>1]=0;
			next_to_delete+=2;
			next_to_delete &= (TXBUF_NUMBER-1);
		}
	}
	
	inline in_frame * get_frame();
};

struct in_frame
{
	emac_driver & driver;
	unsigned from;
	unsigned to;
	rbuf_iterator start()
	{
		return driver.get_rbuf_iterator(from);
	}
	rbuf_iterator end()
	{
		return driver.get_rbuf_iterator(to);
	}
	BYTE * get_byte_start()
	{
		return reinterpret_cast<BYTE*>(driver.rbuffers[from]);
	}
	unsigned size() const
	{
		unsigned last_buf = to ? (to-1) : (RECBUF_NUMBER-1);
		return driver.rx_list[last_buf].status & 2047;
	}
	~in_frame()
	{
		driver.release_rx_bufs(from,to);
	}
	in_frame(unsigned from_buffer, unsigned to_buffer, emac_driver & emac)
		: driver(emac)
		, from(from_buffer)
		, to(to_buffer)
	{
	}
};

inline in_frame * emac_driver::get_frame()	//TODO: handle incomplete frames
{
	unsigned start = next_rx_buf;
	unsigned end = start;
	while(1)
	{
		unsigned current = end;
		if(!(rx_list[current].address & RX_USED))
			break;
		end++;
		end &= (RECBUF_NUMBER-1);
		if(rx_list[current].status & RX_EOF)
			break;
	}
	next_rx_buf = end;
	if(start==end)
		return 0;
	return new in_frame(start,end,*this);
}



#endif	//__emac_helper_h
