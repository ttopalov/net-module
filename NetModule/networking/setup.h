#ifndef _setup_h__
#define _setup_h__

#define RECBUF_NUMBER	(128)	//must be a power of 2
#define TXBUF_NUMBER	(256)	//must be a power of 2
#define OUT_FRAME_NUM	(TXBUF_NUMBER/2)
#define MAX_SOCKET_NUMBER	(256)
#define MAX_SERVER_SOCKET_NUMBER	(16)

#define SHORT_NET_TASK

#endif
