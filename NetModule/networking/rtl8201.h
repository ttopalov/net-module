#ifndef __rtl8201h
#define __rtl8201h

#define RTL8201_BMCR        0   // Basic Mode Control Register
#define RTL8201_BMSR        1   // Basic Mode Status Register
#define RTL8201_PHYID1      2   // PHY Idendifier Register 1
#define RTL8201_PHYID2      3   // PHY Idendifier Register 2
#define RTL8201_ANAR        4   // Auto_Negotiation Advertisement Register
#define RTL8201_ANLPAR      5   // Auto_negotiation Link Partner Ability Register
#define RTL8201_ANER        6   // Auto-negotiation Expansion Register

// Basic Mode Control Register (BMCR)
// Bit definitions: RTL8201_BMCR
#define RTL8201_RESET             (1 << 15) // 1= Software Reset; 0=Normal Operation
#define RTL8201_LOOPBACK          (1 << 14) // 1=loopback Enabled; 0=Normal Operation
#define RTL8201_SPEED_SELECT      (1 << 13) // 1=100Mbps; 0=10Mbps
#define RTL8201_AUTONEG           (1 << 12) // Auto-negotiation Enable
#define RTL8201_POWER_DOWN        (1 << 11) // 1=Power down 0=Normal operation
#define RTL8201_ISOLATE           (1 << 10) // 1 = Isolates 0 = Normal operation
#define RTL8201_RESTART_AUTONEG   (1 << 9)  // 1 = Restart auto-negotiation 0 = Normal operation
#define RTL8201_DUPLEX_MODE       (1 << 8)  // 1 = Full duplex operation 0 = Normal operation
#define RTL8201_COLLISION_TEST    (1 << 7)  // 1 = Collision test enabled 0 = Normal operation
//      Reserved                  6 to 0   // Read as 0, ignore on write

// Basic Mode Status Register (BMSR)
// Bit definitions: RTL8201_BMSR
#define RTL8201_100BASE_T4        (1 << 15) // 100BASE-T4 Capable
#define RTL8201_100BASE_TX_FD     (1 << 14) // 100BASE-TX Full Duplex Capable
#define RTL8201_100BASE_T4_HD     (1 << 13) // 100BASE-TX Half Duplex Capable
#define RTL8201_10BASE_T_FD       (1 << 12) // 10BASE-T Full Duplex Capable
#define RTL8201_10BASE_T_HD       (1 << 11) // 10BASE-T Half Duplex Capable
//      Reserved                  10 to 7  // Read as 0, ignore on write
#define RTL8201_MF_PREAMB_SUPPR   (1 << 6)  // MII Frame Preamble Suppression
#define RTL8201_AUTONEG_COMP      (1 << 5)  // Auto-negotiation Complete
#define RTL8201_REMOTE_FAULT      (1 << 4)  // Remote Fault
#define RTL8201_AUTONEG_ABILITY   (1 << 3)  // Auto Configuration Ability
#define RTL8201_LINK_STATUS       (1 << 2)  // Link Status
#define RTL8201_JABBER_DETECT     (1 << 1)  // Jabber Detect
#define RTL8201_EXTEND_CAPAB      (1 << 0)  // Extended Capability


// Auto-negotiation Advertisement Register (ANAR)
// Auto-negotiation Link Partner Ability Register (ANLPAR)
// Bit definitions: RTL8201_ANAR, RTL8201_ANLPAR
#define RTL8201_NP               (1 << 15) // Next page Indication
#define RTL8201_ACK              (1 << 14) // Acknowledge
#define RTL8201_RF               (1 << 13) // Remote Fault
//      Reserved                12 to 11  // Write as 0, ignore on read
#define RTL8201_FCS              (1 << 10) // Flow Control Support
#define RTL8201_T4               (1 << 9)  // 100BASE-T4 Support
#define RTL8201_TX_FDX           (1 << 8)  // 100BASE-TX Full Duplex Support
#define RTL8201_TX_HDX           (1 << 7)  // 100BASE-TX Support
#define RTL8201_10_FDX           (1 << 6)  // 10BASE-T Full Duplex Support
#define RTL8201_10_HDX           (1 << 5)  // 10BASE-T Support
//      Selector                 4 to 0   // Protocol Selection Bits
#define RTL8201_AN_IEEE_802_3      0x0001

// Auto-negotiation Expansion Register (ANER)
// Bit definitions: RTL8201_ANER
//      Reserved                15 to 5  // Read as 0, ignore on write
#define RTL8201_PDF              (1 << 4) // Local Device Parallel Detection Fault
#define RTL8201_LP_NP_ABLE       (1 << 3) // Link Partner Next Page Able
#define RTL8201_NP_ABLE          (1 << 2) // Local Device Next Page Able
#define RTL8201_PAGE_RX          (1 << 1) // New Page Received
#define RTL8201_LP_AN_ABLE       (1 << 0) // Link Partner Auto-negotiation Able

#endif
