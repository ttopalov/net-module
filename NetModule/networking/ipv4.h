#ifndef __ipv4_h_
#define __ipv4_h_


#include "compiler.h"
#include "emac_helper.h"
#include "setup.h"

inline WORD part_sum(const WORD * data, unsigned count)
{
	unsigned long sum = 0;
	while(count>1)
	{
		WORD tmp = *data;
		sum += tmp;
		data++;
		count-=2;
	}
	if(count)
		sum+=*((BYTE*)data);
	while (sum>>16)
		sum = (sum & 0xffff) + (sum >> 16);
	return sum;
}

inline WORD part_sum(const BYTE * data, unsigned count)
{
	if((reinterpret_cast<unsigned>(data)&1)==0)
		return part_sum(reinterpret_cast<const WORD*>(data),count);
	unsigned long sum = 0;
	while(count>1)
	{
		sum += *data++;
		sum += (*data++)<<8;
		count-=2;
	}
	if(count)
		sum+=(*data);
	while (sum>>16)
		sum = (sum & 0xffff) + (sum >> 16);
	return sum;
}

template<typename T>
inline WORD checksum(const T * data, unsigned count)
{
	WORD sum = part_sum(data,count);
	
	return ~sum;
}

template<typename T1, typename T2>
inline WORD checksum_2(const T1 * data1, unsigned count1, const T2 * data2, unsigned count2)
{
	unsigned long sum = part_sum(data1,count1) + part_sum(data2,count2);
	while (sum>>16)
		sum = (sum & 0xffff) + (sum >> 16);
	return ~sum;
}

template<typename T1, typename T2>
inline WORD continue_checksum(DWORD sum, const T1 * data1, unsigned count1, const T2 * data2, unsigned count2)
{
	sum += part_sum(data1,count1) + part_sum(data2,count2);
	while (sum>>16)
		sum = (sum & 0xffff) + (sum >> 16);
	return ~sum;
}

template<typename T1>
inline WORD continue_checksum(DWORD sum, const T1 * data1, unsigned count1)
{
	sum += part_sum(data1,count1);
	while (sum>>16)
		sum = (sum & 0xffff) + (sum >> 16);
	return ~sum;
}

struct mac_address
{
	BYTE data[6];
	BYTE * copy_to(BYTE * dest) const
	{
		*dest = data[0];
		*++dest = data[1];
		*++dest = data[2];
		*++dest = data[3];
		*++dest = data[4];
		*++dest = data[5];
		return ++dest;
	}
	mac_address & operator=(const mac_address & other)
	{
		other.copy_to(data);
		return *this;
	}
	mac_address()
	{
		data[0]=data[1]=data[2]=data[3]=data[4]=data[5]=0;
	}
	mac_address(const mac_address & other)
	{
		other.copy_to(data);
	}
	mac_address(BYTE o0, BYTE o1, BYTE o2, BYTE o3, BYTE o4, BYTE o5)
	{
		data[0] = o0;
		data[1] = o1;
		data[2] = o2;
		data[3] = o3;
		data[4] = o4;
		data[5] = o5;
	}
	mac_address(const BYTE * octals)
	{
		copy(data,octals, octals+6);
	}
	bool operator==(const mac_address & other) const
	{
		return data[0]==other.data[0] && data[1]==other.data[1] && data[2]==other.data[2] && data[3]==other.data[3] && data[4]==other.data[4] && data[5]==other.data[5]; 
	}
	bool operator!=(const mac_address & other) const
	{
		return !operator==(other);
	}
	bool is_null()
	{
		return data[0]==0 && data[1]==0 && data[2]==0 && data[3]==0 && data[4]==0 && data[5]==0;
	}
}__attribute__((packed));

struct ip_address
{
	BYTE data0;
	BYTE data1;
	BYTE data2;
	BYTE data3;
	inline BYTE * copy_to(BYTE * dest) const
	{
		*dest = data0;
		*++dest = data1;
		*++dest = data2;
		*++dest = data3;
		return ++dest;
	}
	ip_address & operator=(const ip_address & other)
	{
		other.copy_to(&data0);
		return *this;
	}
	ip_address(const ip_address & other)
	{
		other.copy_to(&data0);
	}
	ip_address()
	{
		data0=data1=data2=data3=0;
	}
	ip_address(BYTE o0, BYTE o1, BYTE o2, BYTE o3)
	{
		data0 = o0;
		data1 = o1;
		data2 = o2;
		data3 = o3;
	}
	ip_address(const BYTE * octals)
	{
		copy(&data0, octals, octals+4);
	}
	ip_address(unsigned nones)
	{
		if(nones>32)
			nones=32;
		data0=data1=data2=data3=0;
		BYTE * dat = &data0;
		for(unsigned i=0;i<nones;i++)
			dat[i/8] |= (0x80>>(i%8));
	}
	bool operator==(const ip_address & other) const
	{
		return data0==other.data0 && data1==other.data1 && data2==other.data2 && data3==other.data3;
	}
	bool operator!=(const ip_address & other) const
	{
		return !operator==(other);
	}
	bool is_null()
	{
		return data0==0 && data1==0 && data2==0 && data3==0;
	}
	unsigned long to_long() const
	{
		return ( (data0<<24) | (data1<<16) | (data2<<8) | (data3) );
	}
	bool is_in_net(const ip_address & net, const ip_address & mask)
	{
		return !((to_long() ^ net.to_long()) & mask.to_long());
	}
}__attribute__((packed));

struct frame_header
{
	static const WORD MTU = 1492;
	static const WORD IPV4 = 0x0008;
	static const WORD IPV6 = 0xdd86;
	static const WORD ARP = 0x0608;
	static const WORD RARP = 0x3580;
	mac_address dest_addr;
	mac_address src_addr;
	WORD type;
	BYTE upper_headers[0];
	frame_header(const mac_address & src, WORD frame_type) 
		: src_addr(src)
		, type(frame_type)
	{
	}
}__attribute__((packed));

struct tx_frame : public data
{
	frame_header FrameHeader;
	tx_frame(const BYTE * load, WORD load_size, const mac_address & src, WORD frame_type = 0, WORD headersize = 0) 
		: data(load,load_size,headersize + sizeof(frame_header))
		, FrameHeader(src, frame_type)
	{
	}
	bool is_ready()
	{
		return (!FrameHeader.dest_addr.is_null()) || (FrameHeader.type != frame_header::IPV4);
	}
}__attribute__((packed));

struct arp_header
{
	static const WORD REQUEST = 1<<8;
	static const WORD REPLY = 2<<8;
	static const WORD TYPE_ETHERNET = (1<<8);
	static const WORD TYPE_IPV4 = 0x0008;
	WORD htype;
	WORD ptype;
	BYTE hlen;
	BYTE plen;
	WORD operation;
	mac_address src_mac;
	ip_address src_ip;
	mac_address dest_mac;
	ip_address dest_ip;
	
	arp_header(WORD op, const mac_address & smac, const ip_address & sip, const mac_address dmac, const ip_address dip)
		: htype(TYPE_ETHERNET)	//ethernet
		, ptype(TYPE_IPV4)	//ipv4
		, hlen(6)
		, plen(4)
		, operation(op)
		, src_mac(smac)
		, src_ip(sip)
		, dest_mac(dmac)
		, dest_ip(dip)
	{
	}
}__attribute__((packed));


struct tx_arp_packet : public tx_frame
{
	arp_header ArpHeader;
	tx_arp_packet(WORD op, const mac_address & smac, const ip_address & sip, const mac_address dmac, const ip_address dip)
		: tx_frame(0,0,smac,frame_header::ARP, sizeof(ArpHeader))
		, ArpHeader(op,smac,sip,dmac,dip)
	{
		FrameHeader.dest_addr = dmac;
	}
}__attribute__((packed));

struct ip_header
{
	static const WORD MTU = frame_header::MTU - 20;
	static const BYTE ICMP = 1;
	static const BYTE TCP = 6;
	static const BYTE UDP = 11;
	BYTE version_len;
	BYTE dscp;
	WORD total_len;
	WORD id;
	WORD flags_and_offset;
	BYTE ttl;
	BYTE proto;
	WORD chksum;
	ip_address src;
	ip_address dst;
	BYTE data[0];
	const BYTE * get_data() const
	{
		return data-20+header_len();
	}
	BYTE header_len() const
	{
		return (version_len & 0x0f)<<2;
	}
	ip_header(WORD len, WORD identification, BYTE TTL, WORD protocol, const ip_address & src_ip, const ip_address & dst_ip)
		: version_len((4<<4) | 5)
		, dscp(0)
		, total_len(swap_word(len))
		, id(swap_word(identification))
		, flags_and_offset(0)
		, ttl(TTL)
		, proto(protocol)
		, chksum(0)
		, src(src_ip)
		, dst(dst_ip)
	{
		chksum = checksum(reinterpret_cast<BYTE*>(this),20);
	}
	bool is_ok()	//ip version is 4, ip options are no more than 12 bytes and checksum is correct
	{
		return ((version_len & 0xf0) == 0x40) && ((version_len & 0x0f)<=8) && (chksum == checksum_2(reinterpret_cast<BYTE*>(this),10,reinterpret_cast<BYTE*>(&src),8));
	}
}__attribute__((packed));

struct tx_ip_packet : public tx_frame		//TODO: add fragmentation support
{
	ip_header IpHeader;
	tx_ip_packet(const ip_address & src, const ip_address & dest, const mac_address & smac, const BYTE * load_ptr, WORD load_size, WORD packet_id, BYTE protocol, WORD headersize = 0, BYTE start_ttl = 64)
		: tx_frame(load_ptr, load_size, smac, frame_header::IPV4, sizeof(ip_header)+headersize)
		, IpHeader(load_size+headersize+sizeof(ip_header), packet_id, start_ttl, protocol, src, dest)
	{
	}
}__attribute__((packed));

struct icmp_header
{
	static const BYTE ECHO_REQUEST = 8;
	static const BYTE ECHO_REPLY = 0;
	BYTE type;
	BYTE code;
	WORD chksum;
	DWORD rest_of_header;
	BYTE pad[0];
	icmp_header(BYTE icmp_type, BYTE icmp_code = 0, DWORD RoH = 0)
		: type(icmp_type)
		, code(icmp_code)
		, chksum(0)
		, rest_of_header(RoH)
	{
	}
}__attribute__((packed));

struct tx_icmp_packet : public tx_ip_packet
{
	icmp_header IcmpHeader;
	BYTE pad[64];
	
	tx_icmp_packet(const ip_address & src, const ip_address & dest, const mac_address & smac, WORD len, BYTE * load_ptr, BYTE icmp_type, BYTE icmp_code = 0, DWORD RoH = 0)
		: tx_ip_packet(src, dest, smac, 0, 0, 1, ip_header::ICMP, len + sizeof(icmp_header))
		, IcmpHeader(icmp_type, icmp_code, RoH)
	{
		fast_copy(pad, load_ptr, load_ptr+len);
		IcmpHeader.chksum = checksum(reinterpret_cast<BYTE *>(&IcmpHeader),(len+sizeof(icmp_header)));
	}
}__attribute__((packed));

#endif
