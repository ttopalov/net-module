#ifndef _TCP_H__
#define _TCP_H__

#include "network.h"
#include "romfs/romfs.h"
#include "setup.h"

struct sys_c
{
	ip_address ip;
	ip_address gateway;
	unsigned char mask;
	unsigned char filter;
	unsigned char magic;
//	mac_address mac;
}__attribute__((packed));
struct sys_config
{
	BYTE data[sizeof(sys_c)];
	sys_config & operator=(const sys_config & other)
	{
		copy(data,other.data,other.data+sizeof(sys_c));
		return *this;
	}
};
extern sys_config system_config;// __attribute__ ((section (".config")));
extern char system_password[16];

struct tcp_header
{
	static const WORD MSS = ip_header::MTU - 20;
	
	static const BYTE CWR = (1<<7);
	static const BYTE ECE = (1<<6);
	static const BYTE URG = (1<<5);
	static const BYTE ACK = (1<<4);
	static const BYTE PSH = (1<<3);
	static const BYTE RST = (1<<2);
	static const BYTE SYN = (1<<1);
	static const BYTE FIN = (1<<0);
	WORD sport;
	WORD dport;
	DWORD seq_num;
	DWORD ack_num;
	BYTE offset;
	BYTE flags;
	WORD wnd_size;
	WORD chksum;
	WORD urgent_pointer;
	DWORD options[0];
	inline tcp_header(WORD source_port, WORD dest_port, DWORD seq_number, DWORD ack_number, BYTE flg)
		:sport(source_port)
		,dport(dest_port)
		,seq_num(swap_dword(seq_number))
		,ack_num(swap_dword(ack_number))
		,offset(5<<4)
		,flags(flg)
		,wnd_size(swap_word(MSS))
		,chksum(0)	//chksum(swap_word(checksum(reinterpret_cast<BYTE*>(this),(get_offset()-1)/2)))
		,urgent_pointer(0)
	{
	}
	inline BYTE get_offset() const
	{
		return offset>>4;
	}
/*	bool is_correct()
	{
		return chksum == swap_word(checksum(reinterpret_cast<BYTE*>(this),(get_offset()-1)/2));
	}*/
	inline DWORD get_seq() const
	{
		return swap_dword(seq_num);
	}
	inline DWORD get_ack() const
	{
		return swap_dword(ack_num);
	}
}__attribute__((packed));

struct tx_tcp_chunk : public tx_ip_packet
{
	tcp_header TcpHeader;

	inline tx_tcp_chunk(WORD source_port, WORD dest_port, DWORD seq_number, DWORD ack_number, BYTE flg, const BYTE * load_ptr, WORD load_size, const ip_address & src, const ip_address & dest, const mac_address & smac)
		: tx_ip_packet(src, dest, smac, load_ptr, load_size, 1, ip_header::TCP, sizeof(tcp_header))
		, TcpHeader(source_port, dest_port, seq_number, ack_number, flg)
	{
		TcpHeader.chksum = continue_checksum(swap_word(6+20+load_size), reinterpret_cast<BYTE*>(&TcpHeader)-8,16+8, load_ptr, load_size);
	}
	inline tx_tcp_chunk(WORD source_port, WORD dest_port, DWORD seq_number, DWORD ack_number, BYTE flg, const BYTE * load_ptr, WORD load_size, const ip_address & src, const ip_address & dest, const mac_address & smac, DWORD part_sum)
		: tx_ip_packet(src, dest, smac, load_ptr, load_size, 1, ip_header::TCP, sizeof(tcp_header))
		, TcpHeader(source_port, dest_port, seq_number, ack_number, flg)
	{
		TcpHeader.chksum = continue_checksum(swap_word(6+20+load_size+part_sum), reinterpret_cast<BYTE*>(&TcpHeader)-8,16+8);
	}
};

template<typename T>
struct port_list_ent
{
	T * socket;
	port_list_ent * next;
	inline port_list_ent(T * sock,port_list_ent * older)
		: socket(sock)
		, next(older)
	{
	}
	static int blah;
	static port_list_ent * head;
	static void * operator new(size_t size, const std::nothrow_t nc) throw()
	{
		if(!head)
			return 0;
		port_list_ent * tmp = head;
		head = head->next;
		return tmp;
	}
	void operator delete(void * location, size_t size)
	{
		if(location==0)
			return;
		static_cast<port_list_ent*>(location)->next = head;
		head = static_cast<port_list_ent*>(location);
	}
	static void init(DWORD * raw_data, unsigned count)
	{
		head=0;
		while(count--)
		{
			port_list_ent * nb = reinterpret_cast<port_list_ent*>(raw_data);
			nb->next = head;
			head = nb;
			raw_data+=sizeof(port_list_ent)/sizeof(DWORD);
		}
	}
};
template<typename T>
struct socket_list
{
	typedef port_list_ent<T> port_list_entry;
	port_list_entry * head;
//	WORD next_to_give;
	inline socket_list()
		: head(0)
//		, next_to_give(15000)
	{
	}
	inline bool is_used(WORD port)
	{
		for(port_list_entry * cur = head; cur; cur = cur->next)
			if(cur->socket->local_port == port)
				return true;
		return false;
	}
/*	
	WORD assign_port(T * sock,  WORD rport = 0, WORD lport = 0)
	{
		if(port)
		{
			if(is_used(port))
				return 0;
			head = new port_list_entry(sock,head,lport);
			return port;
		}
		else
		{
			for(;is_used(next_to_give);++next_to_give);
			head = new port_list_entry(sock,head,next_to_give);
			return next_to_give++;
		}
	}*/
	inline void add_sock(T * sock)
	{
		port_list_entry * tmp = new(std::nothrow) port_list_entry(sock,head);
		if(tmp)
			head = tmp;
	}
	inline void remove_sock(T * sock)
	{
		if(sock)
		{
			port_list_entry * prev = 0;
			for(port_list_entry * cur = head; cur; prev = cur, cur = cur->next)
			{
				if(cur->socket == sock)
				{
					if(prev)
						prev->next = cur->next;
					else
						head = cur->next;
					delete cur;
					return;
				}
			}
		}
	}
};

struct tcp_socket
{
	enum socket_type {GENERIC,HTTP,CONFIG};
	enum state_type {LISTEN, SYN_SENT, SYN_RECEIVED, ESTABLISHED, FIN_WAIT_1, FIN_WAIT_2, CLOSE_WAIT, CLOSING, LAST_ACK, TIME_WAIT, CLOSED};
	static network_handler & l3;
	static socket_list<tcp_socket> tcp_sockets;
	
	static tcp_socket * head;
	static void * operator new(size_t size, const std::nothrow_t& nothrow_constant) throw()
	{
		if(!head)
			return 0;
		tcp_socket * tmp = head;
		head = head->next;
		return tmp;
	}
	void operator delete(void * location, size_t size)
	{
		if(location==0)
			return;
		static_cast<tcp_socket*>(location)->next = head;
		head = static_cast<tcp_socket*>(location);
	}
	static void init(DWORD * raw_data, unsigned count)
	{
		head=0;
		while(count--)
		{
			tcp_socket * nb = reinterpret_cast<tcp_socket*>(raw_data);
			nb->next = head;
			head = nb;
			raw_data+=sizeof(tcp_socket)/sizeof(DWORD);
		}
	}

	WORD local_port;
	WORD remote_port;
	WORD remote_window;
	state_type state;
	ip_address remote_ip;
	
	union
	{
		DWORD seq_num;
		tcp_socket * next;
	};
	DWORD ack_num;
	DWORD sent_acked;
	BYTE * recbuf;
	unsigned recbuf_len;
	unsigned rec_num;
	unsigned close_timeout;
	const socket_type type;
	const BYTE * sendbuf;
	unsigned left_to_send;
	bool closing;
	
	inline tcp_socket(WORD lport, WORD rport, state_type initial_state, ip_address rem_ip, socket_type sockettype = GENERIC)
		: local_port(lport)
		, remote_port(rport)
		, remote_window(0)
		, state(initial_state)
		, remote_ip(rem_ip)
		, seq_num(1)
		, ack_num(0)
		, sent_acked(1)
		, recbuf(0)
		, recbuf_len(0)
		, rec_num(0)
		, close_timeout(5)
		, type(sockettype)
		, sendbuf(0)
		, left_to_send(0)
		, closing(false)
	{
		tcp_sockets.add_sock(this);
	}
	inline void send_code(BYTE flags, DWORD seq_number)
	{
		l3.queue_packet(new(std::nothrow) tx_tcp_chunk(local_port, remote_port, seq_number, ack_num, flags | tcp_header::ACK, 0, 0, l3.my_ip, remote_ip, l3.my_mac));
	}
	inline void send_data(const BYTE * load_ptr, WORD load_size)
	{
		l3.queue_packet(new(std::nothrow) tx_tcp_chunk(local_port, remote_port, seq_num, ack_num, tcp_header::PSH | tcp_header::ACK , load_ptr, load_size, l3.my_ip, remote_ip, l3.my_mac));
		seq_num+=load_size;
	}
	inline void send_data(const BYTE * load_ptr, WORD load_size, DWORD data_sum)
	{
		l3.queue_packet(new(std::nothrow) tx_tcp_chunk(local_port, remote_port, seq_num, ack_num, tcp_header::PSH | tcp_header::ACK, load_ptr, load_size, l3.my_ip, remote_ip, l3.my_mac, data_sum));
		seq_num+=load_size;
	}		
	inline void timer_tick()
	{
		try_send();
		if(close_timeout)
			close_timeout--;
		else
		{
			switch(state)
			{
				case FIN_WAIT_1: case FIN_WAIT_2: case CLOSING: case TIME_WAIT: case SYN_RECEIVED: case LAST_ACK:
					state = CLOSED;
					break;
				case ESTABLISHED:
					if(seq_num>sent_acked+1)
					{
						unsigned not_acked = seq_num - sent_acked;
						seq_num-=not_acked;
						try_send();
					}
				default:
					if(closing)
						state = CLOSED;
					break;
			}
		}
	}
	inline static bool delete_closed()
	{
		for(port_list_ent<tcp_socket> * cur =tcp_sockets.head; cur; cur = cur->next)
		{
			if(cur->socket->state == CLOSED)
			{
				delete cur->socket;
				return true;
			}
		}
		return false;
	}
	inline static void on_timer_tick()
	{
		for(port_list_ent<tcp_socket> * cur =tcp_sockets.head; cur; cur = cur->next)
			cur->socket->timer_tick();
		while(delete_closed());
	}
	~tcp_socket()
	{
		tcp_sockets.remove_sock(this);
	}
	
	inline void incoming_chunk(rbuf_iterator chunk, unsigned tcp_len)
	{
		const tcp_header * header = static_cast<const tcp_header*>(chunk.get_pointer());
		BYTE flags = header->flags;
		if(flags & tcp_header::ACK)
		{
			unsigned new_ack = header->get_ack();
			if(new_ack>sent_acked)
			{
				sendbuf += (new_ack - sent_acked);
				sent_acked = new_ack;
				close_timeout=5;
			}
		}
		remote_window = swap_word(header->wnd_size);
		switch(state)
		{
		case LISTEN:
			if(flags & tcp_header::SYN)
			{
				ack_num = header->get_seq()+1;
				send_code(tcp_header::SYN | tcp_header::ACK, seq_num++);
				state = SYN_RECEIVED;
				close_timeout=5;
			}
			break;
		case SYN_SENT:
			if((flags & tcp_header::SYN) && (flags & tcp_header::ACK))
				state = ESTABLISHED;
				close_timeout=5;
			break;
		case SYN_RECEIVED:
			if(flags & tcp_header::ACK)
				state = ESTABLISHED;
				close_timeout=5;
			break;
		case ESTABLISHED:
			if(flags & tcp_header::FIN)
			{
				ack_num = header->get_seq()+1;
				send_code(tcp_header::FIN, seq_num++);
				state = LAST_ACK;
				close_timeout=5;
			}
			else
			{
				if(ack_num == header->get_seq())	//if not, ignore and cause retransmit
				{
					unsigned datalen = tcp_len - header->get_offset()*4;	//TODO: check datalen
					if(!datalen)
						break;
					rbuf_iterator load = chunk + header->get_offset();
					buf_iterator<BYTE> rbuf = load.byte_iterator();
					switch(type)
					{
					case HTTP:
						ack_num += datalen;
						if(rbuf[0]=='G' && rbuf[1]=='E' && rbuf[2]=='T')
						{
//							buf_iterator<BYTE> c = rbuf+3;
							const romfs_entry * file = get_file(rbuf+4);
							send(file->get_data(),file->len);
						}
						else	//not supported
						{
							send_code(tcp_header::ACK, seq_num);
						}
						close();
						break;
					case CONFIG:
						ack_num += datalen;
						if((datalen<(16+1)) || compare_strings(rbuf,system_password,0,16))
						{
							send_code(tcp_header::ACK, seq_num);
							close();
							break;
						}
						rbuf = rbuf + 16;
						switch(rbuf[0])
						{
						case 0xab:
							send((BYTE*)&system_config,sizeof(system_config));
							break;
						case 0xac:
							copy((BYTE*)&system_config,rbuf+1,rbuf+1+sizeof(system_config));
							eeprom_write(AT91C_BASE_SPI0,0,(BYTE*)&system_config,sizeof(system_config));
							send((BYTE*)&system_config,sizeof(system_config));
							break;
						case 0xad:
							copy(system_password, rbuf+1, rbuf+1+sizeof(system_password));
							eeprom_write(AT91C_BASE_SPI0,16,(BYTE*)system_password,sizeof(system_password));
							send((BYTE*)system_password,sizeof(system_password));
							break;
						case 0xae:
							send_code(tcp_header::ACK, seq_num);
							wait_rtt(5);
							reset_cpu();
						default:
							send_code(tcp_header::ACK, seq_num);
						}
						close();
						break;
					default:
						if(datalen>recbuf_len)
							datalen=recbuf_len;
						if(!datalen)
							break;
						ack_num += datalen;
						send_code(tcp_header::ACK, seq_num);
						if((reinterpret_cast<unsigned>(recbuf) & 3) == 0)
							copy(reinterpret_cast<DWORD*>(recbuf), load, load + ((datalen+3)>>2));
						else
							copy(recbuf, load.byte_iterator(), load.byte_iterator()+datalen);
						rec_num+=datalen;
						recbuf_len-=datalen;
						recbuf+=datalen;
						break;
					}
				}
			}
			break;
		case FIN_WAIT_1:
			ack_num = header->get_seq()+1;
			if(flags & (tcp_header::ACK | tcp_header::FIN))
			{
				send_code(tcp_header::ACK, seq_num);
				state = CLOSED; //TIME_WAIT
			}
			else if(flags & tcp_header::FIN)
			{
				send_code(tcp_header::ACK, seq_num);
				state = CLOSING;
			}
			else if(flags & tcp_header::ACK)
			{
				state = FIN_WAIT_2;
			}
			break;
		case FIN_WAIT_2:
			ack_num = header->get_seq()+1;
			if(flags & tcp_header::FIN)
			{
				send_code(tcp_header::ACK, seq_num);
				state = CLOSED; //TIME_WAIT
			}
			break;
		case CLOSE_WAIT:
			break;
		case CLOSING:
			if(flags & tcp_header::ACK)
				state = CLOSED; //TIME_WAIT
			break;
		case LAST_ACK:
			if(flags & tcp_header::ACK)
				state = CLOSE_WAIT;
			break;
		case TIME_WAIT: case CLOSED:
			break;
		}
		try_send();
		try_close();
	}
	inline void try_send()
	{
		if(state!=ESTABLISHED)
			return;
		unsigned available_window = remote_window - (seq_num - sent_acked);
		unsigned toqueue = MIN(available_window, left_to_send);
		left_to_send-=toqueue;
		const BYTE * buf = sendbuf+(seq_num - sent_acked);
		while(toqueue)
		{
			unsigned tosend = MIN(toqueue,tcp_header::MSS);
			send_data(buf, tosend);	
			buf+=tosend;
			toqueue-=tosend;
		}
	}
	inline bool send(const BYTE * buf, unsigned len)
	{
		if(state!=ESTABLISHED || left_to_send || seq_num>sent_acked)
			return false;
		sendbuf = buf;
		left_to_send = len;
		try_send();
		return true;
	}
	inline bool sending()
	{
		return left_to_send;
	}
/*	inline unsigned send(const BYTE * buf, unsigned len, unsigned data_sum)
	{
		if(state!=ESTABLISHED)
			return 0;
		if(len>tcp_header::MSS)		//fragmentation needed, partial checksum is useless
			return send(buf,len);
		send_data(buf, len, data_sum);
		return len;
	}*/
	inline void start_recv(DWORD * buf, unsigned len)
	{
		recbuf = reinterpret_cast<BYTE *>(buf);
		recbuf_len = len;
		rec_num = 0;
	}
	inline unsigned get_rec_num()
	{
		return rec_num;
	}
	inline void stop_recv()
	{
		rec_num = 0;
	}
	inline void close()
	{
		closing = true;
		try_close();
	}
	void try_close()
	{
		if((!closing) || left_to_send || seq_num>sent_acked+1)
			return;
		close_now();
//		closing=0;		
	}
	inline void close_now()
	{
		//TODO: wait for ACKs
		switch(state)
		{
			case LISTEN: case SYN_SENT:
				state=CLOSED;
				break;
			case ESTABLISHED: case SYN_RECEIVED:
				send_code(tcp_header::FIN,seq_num++);
				state = FIN_WAIT_1;
				close_timeout = 5;
				break;
			case CLOSE_WAIT:
				state = CLOSED;
				break;
			default:
				break;
		}
	}
	inline bool is_connected()
	{
		return state == ESTABLISHED;
	}
};

struct server_socket
{
	static const BYTE queue_len = 8;
	static socket_list<server_socket> server_sockets;
	
	tcp_socket * socket_queue[queue_len];
	WORD local_port;
	tcp_socket::socket_type type;
	bool listening;
	
	inline server_socket(tcp_socket::socket_type sock_type = tcp_socket::GENERIC)
		: type(sock_type)
		, listening(false)
	{
		for(unsigned i=0;i<queue_len;i++)
		{
			socket_queue[i]=0;
		}
		server_sockets.add_sock(this);
	}
	inline bool bind(WORD port)
	{
		if(server_sockets.is_used(port))
			return false;
		local_port = port;
		return true;
	}
	inline bool listen()
	{
		listening = !!local_port;
		return listening;
	}
	inline void timer_tick()
	{
	}
	inline static void on_timer_tick()
	{
		for(port_list_ent<server_socket> * cur =server_sockets.head; cur; cur = cur->next)
			cur->socket->timer_tick();
	}
	tcp_socket * accept()
	{
		for(unsigned i=0;i<queue_len;i++)
		{
			if(socket_queue[i] && socket_queue[i]->state == tcp_socket::ESTABLISHED)
			{
				tcp_socket * ret = socket_queue[i];
				socket_queue[i]=0;
				return ret;
			}
		}
		return 0;
	}
	inline void sanitize_queue()
	{
		for(unsigned i=0;i<queue_len;i++)
		{
			if(socket_queue[i])
			{
				switch(socket_queue[i]->state)
				{
					case tcp_socket::CLOSED:
						delete socket_queue[i];
						socket_queue[i]=0;
						break;
					case tcp_socket::ESTABLISHED:
						if(type == tcp_socket::HTTP || type == tcp_socket::CONFIG)
							socket_queue[i]=0;
						break;
					default:
						break;
				}
			}
		}
	}
	inline void incoming_chunk(rbuf_iterator chunk, ip_address rem_ip, unsigned tcp_len)
	{
		sanitize_queue();
		if(!listening)
			return;
		const tcp_header * header = static_cast<const tcp_header*>(chunk.get_pointer());
		
		BYTE flags = header->flags;
		
		if(flags & tcp_header::SYN)
		{
			for(unsigned i=0;i<queue_len;i++)
			{
				if(!socket_queue[i])
				{
					socket_queue[i] = new(std::nothrow) tcp_socket(local_port,header->sport,tcp_socket::LISTEN,rem_ip,type);
					if(socket_queue[i])
						socket_queue[i]->incoming_chunk(chunk, tcp_len);
					return;
				}
			}
		}
		sanitize_queue();
	}
	inline ~server_socket()
	{
		server_sockets.remove_sock(this);
	}
};

struct l4_handler
{
	network_handler l3;
	
	inline l4_handler()
	{
	}
	inline void timer_tick()
	{
		l3.timer_tick();
		tcp_socket::on_timer_tick();
		server_socket::on_timer_tick();
	}
	inline void net_task()
	{
#ifdef SHORT_NET_TASK
		if
#else
		while
#endif
			(in_frame * f = l3.get_ip_packet())
		{
			rbuf_iterator it = f->start();
			const frame_header * fr = reinterpret_cast<const frame_header*>(static_cast<const BYTE*>(it.get_pointer())+2);
			const ip_header * ip = reinterpret_cast<const ip_header*>(fr->upper_headers);
			switch(ip->proto)
			{
			case ip_header::TCP:
				{
					const tcp_header * tcp = reinterpret_cast<const tcp_header*>(ip->get_data());
					
					unsigned tcp_len = swap_word(ip->total_len) - ip->header_len();
					
					
					for(port_list_ent<tcp_socket> * cur = tcp_socket::tcp_sockets.head; cur; cur = cur->next)
					{
						if(cur->socket->local_port == tcp->dport && cur->socket->remote_port == tcp->sport && cur->socket->remote_ip == ip->src)
						{
							cur->socket->incoming_chunk(it+4+(ip->header_len()>>2), tcp_len);
							break;
						}
					}

					for(port_list_ent<server_socket> * cur = server_socket::server_sockets.head; cur; cur = cur->next)
					{
						if(cur->socket->local_port == tcp->dport)
						{
							cur->socket->incoming_chunk(it+4+(ip->header_len()>>2), ip->src, tcp_len);
							break;
						}
					}
				}
				break;
			case ip_header::UDP:
				break;
			default:
				break;
			}
			delete f;
		}
	}
	inline void init(unsigned phy_addr, const mac_address & mac_addr, const ip_address & ip_addr, const ip_address & mask, const ip_address & gw)
	{
		l3.init(phy_addr, mac_addr, ip_addr, mask, gw);
	}
};


#endif
