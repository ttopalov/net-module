#ifndef __network_h_
#define __network_h_

#include "ipv4.h"
#include "setup.h"

struct arp_entry
{
	static const unsigned MAX_ENTRIES = 64;
	static const unsigned timeout = 30;
	ip_address ip;
	mac_address mac;
	arp_entry * next;
	unsigned valid;
	arp_entry(const ip_address & ip_addr, const mac_address & mac_addr, arp_entry * older)
		: ip(ip_addr)
		, mac(mac_addr)
		, next(older)
		, valid(timeout)
	{
	}
	static arp_entry * head;
	static void * operator new(size_t size, const std::nothrow_t nc) throw()
	{
		if(!head)
			return 0;
		arp_entry * tmp = head;
		head = head->next;
		return tmp;
	}
	void operator delete(void * location, size_t size)
	{
		if(location==0)
			return;
		static_cast<arp_entry*>(location)->next = head;
		head = static_cast<arp_entry*>(location);
	}
	static void init(BYTE * raw_data, unsigned count)
	{
		head=0;
		while(count--)
		{
			arp_entry * nb = reinterpret_cast<arp_entry*>(raw_data);
			nb->next = head;
			head = nb;
			raw_data+=sizeof(arp_entry);
		}
	}
}__attribute__((packed));

struct arp_cache
{
	arp_entry * head;
	arp_cache()
		: head(0)
	{
	}
	void delete_entry(const ip_address & ip)
	{
		arp_entry * entry = 0;
		arp_entry * prev = 0;
		for(entry = head; entry ; prev = entry, entry = entry->next)
		{
			if(entry->ip == ip)
			{
				if(prev)
					prev->next = entry->next;
				else
					head = entry->next;
				delete entry;
				return;
			}
		}
	}
	void add_entry(const ip_address & ip, const mac_address & mac)
	{
		delete_entry(ip);
		sanitize();
		arp_entry * entry = new(std::nothrow) arp_entry(ip, mac, head);
		if(entry)
			head = entry;
	}
	mac_address get_entry(const ip_address & ip) const
	{
		for(arp_entry * entry = head; entry; entry = entry->next)
		{
			if(ip == entry->ip)
				return entry->mac;
		}
		return mac_address(0,0,0,0,0,0);
	}
	void sanitize(bool timer_tick = 0)
	{
		arp_entry * entry = 0;
		arp_entry * prev = 0;
		unsigned len = arp_entry::MAX_ENTRIES-1;
		if(timer_tick)
			for(entry = head; entry && (--len) && (--entry->valid); prev = entry, entry = entry->next);
		else
			for(entry = head; entry && (--len); prev = entry, entry = entry->next);
		if(prev)
			prev->next = 0;
		else
			head=0;
		while(entry)
		{
			prev = entry;
			entry = entry->next;
			delete prev;
		}
	}
};

struct pq_entry
{
	static const unsigned MAX_ENTRIES = 16;
	pq_entry * next;
	tx_ip_packet * packet;
	unsigned valid;
	pq_entry(tx_ip_packet * p)
		: next(0)
		, packet(p)
	{
	}
	static pq_entry * head;
	static void * operator new(size_t size, const std::nothrow_t nc) throw()
	{
		if(!head)
			return 0;
		pq_entry * tmp = head;
		head = head->next;
		return tmp;
	}
	void operator delete(void * location, size_t size)
	{
		if(location==0)
			return;
		static_cast<pq_entry*>(location)->next = head;
		head = static_cast<pq_entry*>(location);
	}
	static void init(DWORD * raw_data, unsigned count)
	{
		head=0;
		while(count--)
		{
			pq_entry * nb = reinterpret_cast<pq_entry*>(raw_data);
			nb->next = head;
			head = nb;
			raw_data+=sizeof(pq_entry)/sizeof(DWORD);
		}
	}
};

struct packet_queue
{
	pq_entry * head;
	
	packet_queue()
		: head(0)
	{
	}
	void add_packet(tx_ip_packet * packet)
	{
		sanitize();
		if(!head)
			head = new(std::nothrow) pq_entry(packet);
		else
		{
			pq_entry * tail = head;
			for(;tail->next;tail = tail->next);
			tail->next = new(std::nothrow) pq_entry(packet);
		}
	}
	void sanitize(bool timer_tick = 0)
	{
		unsigned len = 0;
		if(timer_tick)
		{
			for(pq_entry * cur = head; cur; cur = cur->next)
			{
				cur->valid--;
				len++;
			}
		}
		else
		{
			for(pq_entry * cur = head; cur; cur = cur->next)
				len++;
		}
		for(pq_entry * cur = head; (len-- > pq_entry::MAX_ENTRIES-1) || (cur && (!cur->valid)); cur = head)
		{
			head = cur->next;
			delete cur->packet;
			delete cur;
		}
	}
	tx_ip_packet * get_ready_packet(const arp_cache & arp, const ip_address & my_ip, const ip_address & my_mask, const ip_address & gw_ip)	//TODO: pass non-ip packets
	{
		pq_entry * prev = 0;
		for(pq_entry * entry = head; entry ; prev = entry, entry = entry->next)
		{
//			mac_address dmac = arp.get_entry(entry->packet->IpHeader.dst);		
			ip_address dip = entry->packet->IpHeader.dst;
			if(!dip.is_in_net(my_ip, my_mask))
				dip = gw_ip;
			mac_address dmac = arp.get_entry(dip);
			if(!dmac.is_null())
			{
				if(prev)
					prev->next = entry->next;
				else
					head = entry->next;
				tx_ip_packet * ret = entry->packet;
				delete entry;
				ret->FrameHeader.dest_addr = dmac;
				return ret;
			}
		}
		return 0;
	}
};

struct network_handler
{
	emac_driver emac;
	mac_address my_mac;
	ip_address my_ip;
	ip_address my_mask;
	ip_address gw_ip;
	arp_cache arp;
	packet_queue waiting_packets;
	
	network_handler()
	{
	}
	void init(unsigned phy_addr, const mac_address & mac_addr, const ip_address & ip_addr, const ip_address & mask, const ip_address & gw)
	{
		my_mac = mac_addr;
		my_ip = ip_addr;
		my_mask = mask;
		gw_ip = gw;
		emac.init(phy_addr,my_mac.data);
	}
	void queue_packet(tx_ip_packet * packet)
	{
		if(!packet)
			return;
		if(packet->IpHeader.src.is_null())
			packet->IpHeader.src = my_ip;
		ip_address dip = packet->IpHeader.dst;
		if(!dip.is_in_net(my_ip, my_mask))
			dip = gw_ip;
		mac_address mac = arp.get_entry(dip);
		if(!mac.is_null())
		{
			packet->FrameHeader.dest_addr = mac;
			emac.queue_frame(packet);
		}
		else
		{
			waiting_packets.sanitize();
			waiting_packets.add_packet(packet);
			emac.queue_frame(new(std::nothrow) tx_arp_packet(arp_header::REQUEST, my_mac, my_ip, mac_address(255,255,255,255,255,255), dip));
		}
	}
	void timer_tick()
	{
		arp.sanitize(1);
	}
	void send_queued_packets()
	{
		while(tx_ip_packet * packet = waiting_packets.get_ready_packet(arp, my_ip, my_mask, gw_ip))
		{
			emac.queue_frame(packet);
		}
	}
	
	void net_task()
	{
		emac.delete_sent();
		send_queued_packets();
	}
	
	in_frame * get_ip_packet()
	{
		net_task();
#ifdef SHORT_NET_TASK
		if
#else
		while
#endif
			(in_frame * fr = emac.get_frame())
		{			
			frame_header * f = reinterpret_cast<frame_header*>(fr->get_byte_start()+2);

			switch(f->type)
			{
				case frame_header::IPV4:
				{
					ip_header * ip = reinterpret_cast<ip_header*>(f->upper_headers);
					if(!ip->is_ok())
						break;
					if(!(my_ip==ip->dst))
						break;
					if(ip->proto == ip_header::ICMP)
					{
						icmp_header * ich = reinterpret_cast<icmp_header*>(ip->data);
						if(ich->type == icmp_header::ECHO_REQUEST)
						{
							unsigned pad_len = swap_word(ip->total_len)-ip->header_len()-8;
							if(pad_len>64)
								pad_len=64;
							queue_packet(new(std::nothrow) tx_icmp_packet(my_ip, ip->src, my_mac, pad_len, ich->pad, icmp_header::ECHO_REPLY, 0, ich->rest_of_header));
						}
					}
					else
					{
						return fr;
					}
				}
					break;
				case frame_header::ARP:
				{
//					Send_Msg("\r\nARP in ",9);
					arp_header * ap = reinterpret_cast<arp_header*>(f->upper_headers);
					if(ap->htype == arp_header::TYPE_ETHERNET && ap->ptype == arp_header::TYPE_IPV4 && my_ip==ap->dest_ip)
					{
						switch(ap->operation)
						{
						case arp_header::REQUEST:
							emac.queue_frame(new(std::nothrow) tx_arp_packet(arp_header::REPLY, my_mac, my_ip, ap->src_mac, ap->src_ip));
							break;
						case arp_header::REPLY:
							arp.add_entry(ap->src_ip,ap->src_mac);
							break;
						}
					}
				}
					break;
				default:
					break;
			}
			delete fr;
		}
		return 0;
	}
};


#endif
