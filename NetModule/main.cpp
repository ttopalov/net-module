#include "board.h"
#include "myio.h"

#include "compiler.h"


#include "networking/tcp.h"
#include "mylib.h"


sys_config system_config = {{192,168,1,44, 192,168,1,1, 24, 15, 0xab}};
char system_password[16] = "neeslojna";

counter<8> cnt;

l4_handler network;
network_handler & tcp_socket::l3 = network.l3;
socket_list<server_socket> server_socket::server_sockets;
socket_list<tcp_socket> tcp_socket::tcp_sockets;


////////////// memory pools	////////////////////
DWORD out_data[(data::bufsize/4)*OUT_FRAME_NUM];
data * data::head = 0;
DWORD socket_data[(sizeof(tcp_socket)/4)*MAX_SOCKET_NUMBER];
tcp_socket * tcp_socket::head = 0;
BYTE arp_data[sizeof(arp_entry)*arp_entry::MAX_ENTRIES];
arp_entry * arp_entry::head = 0;
DWORD pq_data[(sizeof(pq_entry)/4)*pq_entry::MAX_ENTRIES];
pq_entry * pq_entry::head = 0;

DWORD ss_list_data[(sizeof(port_list_ent<server_socket>)/4)*MAX_SERVER_SOCKET_NUMBER];
DWORD cs_list_data[(sizeof(port_list_ent<tcp_socket>)/4)*MAX_SOCKET_NUMBER];
template<typename T> port_list_ent<T> * port_list_ent<T>::head=0;

static void mempools_init()
{
	data::init(out_data, OUT_FRAME_NUM);
	tcp_socket::init(socket_data, MAX_SOCKET_NUMBER);
	arp_entry::init(arp_data, arp_entry::MAX_ENTRIES);
	pq_entry::init(pq_data, pq_entry::MAX_ENTRIES);
	port_list_ent<server_socket>::init(ss_list_data, MAX_SERVER_SOCKET_NUMBER);
	port_list_ent<tcp_socket>::init(cs_list_data, MAX_SOCKET_NUMBER);
}

unsigned prev_io = 0;
static void pio_check()
{
	// take the readings and offset it
	unsigned pv = AT91C_BASE_PIOA->PIO_PDSR;
	pv >>= 8;

	//normal counters
	cnt.update_values(pv & (CNT_INPUTS_NORMAL >> 8));
	
	//directional counters
	unsigned diff = (prev_io ^ pv) & 0x40;
	prev_io = pv;
	if(diff & pv) //positive pulse
	{
		//incrementing different timers depending on the direction
		cnt.inc((pv & 0x20) ? 6 : 5);
	}
}

static void InitPeriphery(void) 
{
  //enable the clock of the PIO
  AT91C_BASE_PMC->PMC_PCER = 1 << AT91C_ID_PIOA;
  AT91C_BASE_PMC->PMC_PCER = 1 << AT91C_ID_PIOB;

  AT91C_BASE_PIOA->PIO_PER = OUTPUTS | OUTPUTS_A | INPUTS | CNT_INPUTS;
  UP(AT91C_PA15_SPI0_NPCS3);
  AT91C_BASE_PIOA->PIO_OER = OUTPUTS | OUTPUTS_A;
  AT91C_BASE_PIOA->PIO_ODR = CNT_INPUTS;
  AT91C_BASE_PIOA->PIO_PPUER = IN_DEFAULT;
  AT91C_BASE_PIOA->PIO_PPUDR = CNT_INPUTS;
  
//set some pins to peripheral usage  
  AT91C_BASE_PIOA->PIO_PDR = SPI0_PINS;
  AT91C_BASE_PIOA->PIO_ASR = SPI0_PINS;
//  AT91C_BASE_PIOA->PIO_MDER = SPI0_PINS;//I2C_PINS;	//open drain
//  AT91C_BASE_PIOA->PIO_PPUER = I2C_PINS;	//pull-up
 
  AT91C_BASE_PIOB->PIO_PDR = EMPINS;
  AT91C_BASE_PIOB->PIO_ASR = EMPINS;
  
  AT91C_BASE_PIOB->PIO_PER = OUTPUTS_B;
  AT91C_BASE_PIOB->PIO_OER = OUTPUTS_B;
  //enable the clock of the TWI, SPI
  AT91C_BASE_PMC->PMC_PCER = (1<<AT91C_ID_SPI0);

}

static void configureTimer() //should be at about 2.7ms
{
	AT91C_BASE_PMC->PMC_PCER = (1<<AT91C_ID_TC0);
	AT91PS_TC timer = AT91C_BASE_TC0;
//	timer->TC_CMR[0] = AT91C_TC_CPCTRG;

	timer->TC_CCR = AT91C_TC_CLKEN;
	timer->TC_CCR = AT91C_TC_SWTRG;
}

static bool checkTimer()
{
	return AT91C_BASE_TC0->TC_SR & AT91C_TC_COVFS;
}

#define RTPRES 0x00002000
//#define RTPRES 0x00008000
static void configure_rtt()
{
	AT91C_BASE_RTTC->RTTC_RTMR = RTPRES | AT91C_RTTC_RTTRST;
}

static const unsigned output_map[] = {(1<<6),(1<<7),(1<<4),(1<<5),(1<<3),(1<<2),(1<<0),(1<<1),(1<<29),(1<<30),(1<<27),(1<<28),(1<<25),(1<<26),(1<<23),(1<<24)};

struct cnt_server
{
	union
	{
		DWORD inbuf[32];
		BYTE inbuf_byte[128];
	};
	union
	{
		DWORD outbuf_dword[32];
		BYTE outbuf[128];
	};
	tcp_socket * sock;
	enum states {INITIAL,WAIT_REQUEST};
	states state;
	enum request_types {GET_ONE=1, GET_ALL=2, DEL_ONE=3, DEL_ALL=4, SET_OUT = 5, CLEAR_OUT = 6};

	cnt_server()
		: sock(0)
		, state(INITIAL)
	{
	}
	void init(tcp_socket * nsock)
	{
		sock=nsock;
		state = INITIAL;
	}
	void be_usefull()
	{
		if(sock)
		{
			if(sock->state != tcp_socket::ESTABLISHED)
			{
				sock->close();
	//			sock->state = tcp_socket::CLOSED;
				sock=0;
				return;
			}
			switch(state)
			{
			case INITIAL:
				sock->start_recv(inbuf,sizeof(inbuf));
				outbuf[0] = 0x55;
				sock->send(outbuf,1);
				state = WAIT_REQUEST;
				break;
			case WAIT_REQUEST:
				if(sock->get_rec_num()>=3 && !sock->sending() && inbuf_byte[0]==0xab)
				{
					switch(inbuf_byte[1])
					{
					case GET_ONE:
						outbuf_dword[0] = cnt.get_value(inbuf_byte[2]);
						sock->send(outbuf,sizeof(DWORD));
						sock->start_recv(inbuf,sizeof(inbuf));
						break;
					case DEL_ONE:
						outbuf[0] = DEL_ONE;
						cnt.reset_value(inbuf_byte[2]);
						sock->send(outbuf,1);
						sock->start_recv(inbuf,sizeof(inbuf));
						break;
					case DEL_ALL:
						outbuf[0] = DEL_ALL;
						cnt.reset_all();
						sock->send(outbuf,1);
						sock->start_recv(inbuf,sizeof(inbuf));
						break;
					case SET_OUT:
						{
							unsigned numtoset = inbuf_byte[2] & 15;
							if(numtoset<12)	//PIOA
								AT91C_BASE_PIOA->PIO_SODR = output_map[numtoset];
							else		//PIOB
								AT91C_BASE_PIOB->PIO_SODR = output_map[numtoset];
						}
						outbuf[0] = SET_OUT;
						sock->send(outbuf,1);
						sock->start_recv(inbuf,sizeof(inbuf));
						break;
					case CLEAR_OUT:
						{
							unsigned numtoset = inbuf_byte[2] & 15;
							if(numtoset<12)	//PIOA
								AT91C_BASE_PIOA->PIO_CODR = output_map[numtoset];
							else		//PIOB
								AT91C_BASE_PIOB->PIO_CODR = output_map[numtoset];
						}
						outbuf[0] = CLEAR_OUT;
						sock->send(outbuf,1);
						sock->start_recv(inbuf,sizeof(inbuf));
						break;
					default:
						sock->close();
						sock=0;
					}
				}
				break;
			}
		}
	}
};

int main ( void )
{
	mempools_init();
	// Enable User Reset and set its minimal assertion to 960 us
	AT91C_BASE_RSTC->RSTC_RMR = AT91C_RSTC_URSTEN | (0x4<<8) | (unsigned int)(0xA5<<24);

	InitPeriphery();
	
	configure_rtt();
	configureTimer();
	
	init_SPI(AT91C_BASE_SPI0);
	
//	for(unsigned lv =  AT91C_BASE_RTTC->RTTC_RTVR + 12; lv >  AT91C_BASE_RTTC->RTTC_RTVR;);
	if(READ_IN(IN_DEFAULT))
	{
		sys_config rsc;
		eeprom_read(AT91C_BASE_SPI0,0,(BYTE*)&rsc,sizeof(rsc));
		if(((sys_c*)&rsc)->magic==0xab)
			system_config = rsc;
	
		char rp[16];
		eeprom_read(AT91C_BASE_SPI0,16,(BYTE*)rp,16);
		if(rp[0]!=0xff)
			copy(system_password, rp, rp+16);
	}
	
//	ResetPhy();
//	dump_phy_regs();
	
//	auto_neg();
//	EMAC_DisableMdio();

	{
		mac_address rmac;
		eeprom_read(AT91C_BASE_SPI0,0xfa,rmac.data,6);
		sys_c * sc = (sys_c*)&system_config;
		network.init(31, rmac, sc->ip, ip_address(sc->mask), sc->gateway);

		cnt.setFilterTicks(sc->filter);
	}
	
	server_socket server_sock(tcp_socket::HTTP);
	server_sock.bind(swap_word(80));
	server_sock.listen();
	
	server_socket config_sock(tcp_socket::CONFIG);
	config_sock.bind(swap_word(11880));
	config_sock.listen();
	
	server_socket counter_sock;
	counter_sock.bind(swap_word(11130));
	counter_sock.listen();
	
	cnt_server count_servers[4];
	
	unsigned long lastval = 0;
	while (1)
	{
//		eeprom_read(AT91C_BASE_SPI0,0xfa,rmac.data,6);
		bool timer_tick = AT91C_BASE_RTTC->RTTC_RTVR != lastval;
		if(timer_tick)
		{
			lastval = AT91C_BASE_RTTC->RTTC_RTVR;
		}
		pio_check();
		network.net_task();
		
		for(unsigned i=0;i<sizeof(count_servers)/sizeof(cnt_server);i++)
		{
			if(count_servers[i].sock)
				count_servers[i].be_usefull();
			else if(tcp_socket * sock = counter_sock.accept())
				count_servers[i].init(sock);
		}
		
		if(timer_tick)
		{
			network.timer_tick();
		}
		if (checkTimer())
		{
			cnt.timer_tick();
		}
	}
}
