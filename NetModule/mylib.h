#ifndef _MYLIB
#define _MYLIB

template<unsigned N>
class counter
{
	unsigned temp;
	unsigned prevBits;
	unsigned filterTicks;
	unsigned data[N];
	unsigned ticks[N];

public:
	counter()
		: temp(0)
		, prevBits(0)
		, filterTicks(0)
	{
		reset_all();
	}
	void setFilterTicks(unsigned ft)
	{
		filterTicks = ft;
	}
	void timer_tick()
	{
		for (unsigned i = 0; i < N; i++)
		{
			if (ticks[i])
				ticks[i]--;
		}
	}
	void update_values(DWORD bits)
	{
		unsigned changed = bits ^ prevBits;
		if (changed)
		{
			for (unsigned i = 0; i < N; i++)
			{
				if (!(changed & (1 << i)))
					continue;
				if (bits & (1 << i))	// 0 to 1
				{
					if (!ticks[i])
						data[i]++;
				}
				else // 1 to 0
				{
					ticks[i] = filterTicks;
				}
			}
		}
		prevBits = bits;
	}
	void inc(unsigned n) {
		data[n % N]++;
	}
	unsigned get_value(unsigned n) const
	{
		return data[n % N];
	}
	unsigned reset_value(unsigned n)
	{
		unsigned ret = data[n % N];
		data[n % N] = 0;
		temp &= (~(1 << n));
		return ret;
	}
	void reset_all()
	{
		for(unsigned i = 0; i < N; i++)
			data[i] = 0;
	}
};

#endif
