#ifndef __buffer_manager_
#define __buffer_manager_
#include <new>

//template<unsigned bufsize>
struct buffer
{
	static const unsigned bufsize = 1536;
	union
	{
		DWORD data[bufsize/sizeof(DWORD)];
		WORD wdata[bufsize/sizeof(WORD)];
		BYTE bdata[bufsize/sizeof(BYTE)];
		buffer * next;
	};
	
	static buffer * head;
	static void * operator new(size_t size)
	{
		if(!head)
//			return 0;
			throw new std::bad_alloc;
		buffer * tmp = head;
		head = head->next;
		return tmp;
	}
	void operator delete(void * location, size_t size)
	{
		if(location==0)
			return;
		static_cast<buffer*>(location)->next = head;
		head = static_cast<buffer*>(location);
	}
	static void init(DWORD * raw_data, unsigned count)
	{
		head=0;
		while(count--)
		{
			buffer * nb = reinterpret_cast<buffer*>(raw_data);
			nb->next = head;
			head = nb;
			raw_data+=bufsize/sizeof(DWORD);
		}
	}
};

typedef buffer out_buffer;

#endif
