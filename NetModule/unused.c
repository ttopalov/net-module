//void uart_rx ()
//{
//	volatile short pom = AT91C_BASE_US1->US_RHR;
//}

void Send_Msg(const char * msg, int len)
{
	while(!AT91F_US_SendFrame(AT91C_BASE_US1,(char *)msg, len,0,0));
}

void Send_Reg(unsigned long reg, int len)
{
	char * s = get_next_buf();
	for(int i=len;i--;)
	{
		s[i] = (reg&1) ? '1' : '0';
		reg>>=1;
	}
	while(!AT91F_US_SendFrame(AT91C_BASE_US1,s, len,0,0));
//	while(!USART_WriteBuffer(pUS1,(void*)s,len));
}
void Send_Reg_Hex(unsigned long reg, int len)
{
	char * s = get_next_buf();
	for(int i=len;i--;)
	{
		s[i] = (reg&0xf) + '0';
		if(s[i]>'9')
			s[i]+=('A'-'9'-1);
		reg>>=4;
	}
	while(!AT91F_US_SendFrame(AT91C_BASE_US1,s, len,0,0));
//	while(!USART_WriteBuffer(pUS1,(void*)s,len));
}

static void dump_phy_regs()
{
	MSG("\r\nregister dupm:");
	for(unsigned i = 0; i< sizeof(phy_regs);i++)
	{
		unsigned int rr;
		if(!EMAC_ReadPhy(31,phy_regs[i],&rr,0))
			MSG("register read failed ");
		else
		{
			MSG(bmsg);
			Send_Reg(rr,16);
			MSG(hmsg);
			Send_Reg_Hex(rr,4);
		}
	}
}

static void dump_buffer(BYTE * buf, unsigned len)
{
	char * b = get_next_buf();
	char * s = b;
	*s++ = '\r';
	*s++ = '\n';
	for(unsigned i=0;i<len;i++)
	{
		*s = ((buf[i]>>4)&0xf) + '0';
		if(*s>'9')
			*s +=('A'-'9'-1);
		*++s = (buf[i]&0xf) + '0';
		if(*s>'9')
			*s +=('A'-'9'-1);
		*++s = ' ';
		++s;
	}
	while(!AT91F_US_SendFrame(pUS1,b, b-s,0,0));
}

static unsigned char ResetPhy()
{
    unsigned int bmcr = RTL8201_RESET;
    unsigned int timeout = 10;
    unsigned char ret = 1;

    MSG("RTL8201_ResetPhy\n\r");

//    EMAC_EnableMdio();
    bmcr = RTL8201_RESET;
    EMAC_WritePhy(31, RTL8201_BMCR, bmcr, 0);

    do {
        EMAC_ReadPhy(31, RTL8201_BMCR, &bmcr, 0);
        timeout--;
    } while ((bmcr & RTL8201_RESET) && timeout);

//    EMAC_DisableMdio();

    if (!timeout) {
        ret = 0;
		MSG("Timeout");
    }

    return( ret );
}

static unsigned char auto_neg()
{
	unsigned int value;
//	ResetPhy();
	EMAC_ReadPhy(31, RTL8201_BMCR, &value, 0);
	value &= ~RTL8201_AUTONEG;   // Remove autonegotiation enable
    value &= ~(RTL8201_LOOPBACK|RTL8201_POWER_DOWN);
    value |=  RTL8201_ISOLATE;   // Electrically isolate PHY
    EMAC_WritePhy(31, RTL8201_BMCR, value, 0);
	EMAC_ReadPhy(31, RTL8201_BMCR, &value, 0);
	value |= RTL8201_SPEED_SELECT | RTL8201_AUTONEG | RTL8201_DUPLEX_MODE;
	EMAC_WritePhy(31, RTL8201_BMCR, value, 0);
	EMAC_ReadPhy(31, RTL8201_BMCR, &value, 0);
	MSG("BMCR is: ");
	Send_Reg(value,16);
	// Restart Auto_negotiation
    value |=  RTL8201_RESTART_AUTONEG;
    value &= ~RTL8201_ISOLATE;
	EMAC_WritePhy(31, RTL8201_BMCR, value, 0);
	MSG("\r\nwaiting for autoneg");
	// Check AutoNegotiate complete
    while (1) {

        EMAC_ReadPhy(31, RTL8201_BMSR, &value, 0);
        
        // Done successfully
        if (value & RTL8201_AUTONEG_COMP) {

            MSG("\n\rAutoNegotiate complete");
            break;
        }
		else
		{
//			MSG("\r\nBMSR is: ");
	//		Send_Reg(value,16);
		}
	}
	
	unsigned int phyAnalpar, phyAnar;
	EMAC_ReadPhy(31, RTL8201_ANAR, &phyAnar, 0);
	// Get the AutoNeg Link partner base page
    EMAC_ReadPhy(31, RTL8201_ANLPAR, &phyAnalpar, 0);

    // Setup the EMAC link speed
    if ((phyAnar & phyAnalpar) & RTL8201_TX_FDX) {

        // set MII for 100BaseTX and Full Duplex
        EMAC_SetLinkSpeed(1, 1);
    }
    else if ((phyAnar & phyAnalpar) & RTL8201_10_FDX) {

        // set MII for 10BaseT and Full Duplex
        EMAC_SetLinkSpeed(0, 1);
    }
    else if ((phyAnar & phyAnalpar) & RTL8201_TX_HDX) {

        // set MII for 100BaseTX and half Duplex
        EMAC_SetLinkSpeed(1, 0);
    }
    else if ((phyAnar & phyAnalpar) & RTL8201_10_HDX) {

        // set MII for 10BaseT and half Duplex
        EMAC_SetLinkSpeed(0, 0);
    }
	return 1;
}