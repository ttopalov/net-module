#ifndef _romfs_h__
#define _romfs_h__

//#include <string.h>
#include "../compiler.h"
#include <board.h>

struct romfs_entry
{
	char name[116];
	unsigned int offset;
	unsigned int len;
	unsigned int part_sum;
	const unsigned char * get_data() const;
};

extern const romfs_entry files[] ;
extern const unsigned char filedata[];

template<typename iter>
inline const romfs_entry * get_file(iter filename)
{
	const romfs_entry * ent;
	for(ent = files; ent->name[0]; ++ent)
	{
		if(!compare_strings(ent->name, filename))
			break;
	}
	return ent;
}

inline const unsigned char * romfs_entry::get_data() const
{
	return filedata+offset;
}
/*
extern bool efc_write_page(DWORD * dest_addr, unsigned dwords = 64) FASTRUN;
extern DWORD efc_page_buffer[64];

inline void prepare_efc_buffer(const void * paddr)
{
	DWORD * ad = (DWORD*) ((1023<<8)&(unsigned)paddr);
	for(unsigned i=0;i<64;i++)
		efc_page_buffer[i] = ad[i];
}

template<typename it>
inline void write_to_single_page(const void * addr, it src, unsigned count)
{
	prepare_efc_buffer(addr);
	copy((BYTE*)efc_page_buffer,src,src+count);
	efc_write_page((DWORD*)addr);
}

template<typename it>
inline void write_to_flash(const void * dst, it source, unsigned count)	//must be a BYTE iterator
{
	unsigned dest = (unsigned)dst;
	while(count)
	{
		unsigned paddr = dest&(1023<<8);
		unsigned iaddr = dest&0xff;
		for(unsigned i=0;i<64;i++)
			efc_page_buffer[i] = ((DWORD*)paddr)[i];
		unsigned pc = MIN(255-iaddr,count);
		count-=pc;
		dest+=pc;
		BYTE * pb = (BYTE *)efc_page_buffer;
		while(pc--)
		{
			pb[iaddr++] = *source;
			++source;
		}
		efc_write_page((DWORD*)paddr);
	}
}

*/

#endif
