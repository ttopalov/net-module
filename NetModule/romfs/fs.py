import os

header_ok = "HTTP/1.0 200 OK\r\nContent-Length: %d\r\nConnection: close\r\nContent-Type: %s\r\n\r\n"
header_404 = "HTTP/1.0 404 Object Not Found\r\nContent-Length: %d\r\nConnection: close\r\nContent-Type: text/html\r\n\r\n"
content_404 = "<html><body><h1>HTTP/1.0 404 Object Not Found</h1></body></html>"
all_404 = header_404%len(content_404) + content_404
mime_types = {"ai":"application/postscript", "aif":"audio/x-aiff", "aifc":"audio/x-aiff", "aiff":"audio/x-aiff", "asc":"text/plain", "atom":"application/atom+xml", "au":"audio/basic", "avi":"video/x-msvideo", "bcpio":"application/x-bcpio", "bin":"application/octet-stream", "bmp":"image/bmp", "cdf":"application/x-netcdf", "cgm":"image/cgm", "class":"application/octet-stream", "cpio":"application/x-cpio", "cpt":"application/mac-compactpro", "csh":"application/x-csh", "css":"text/css", "dcr":"application/x-director", "dif":"video/x-dv", "dir":"application/x-director", "djv":"image/vnd.djvu", "djvu":"image/vnd.djvu", "dll":"application/octet-stream", "dmg":"application/octet-stream", "dms":"application/octet-stream", "doc":"application/msword", "dtd":"application/xml-dtd", "dv":"video/x-dv", "dvi":"application/x-dvi", "dxr":"application/x-director", "eps":"application/postscript", "etx":"text/x-setext", "exe":"application/octet-stream", "ez":"application/andrew-inset", "gif":"image/gif", "gram":"application/srgs", "grxml":"application/srgs+xml", "gtar":"application/x-gtar", "hdf":"application/x-hdf", "hqx":"application/mac-binhex40", "htm":"text/html", "html":"text/html", "ice":"x-conference/x-cooltalk", "ico":"image/x-icon", "ics":"text/calendar", "ief":"image/ief", "ifb":"text/calendar", "iges":"model/iges", "igs":"model/iges", "jnlp":"application/x-java-jnlp-file", "jp2":"image/jp2", "jpe":"image/jpeg", "jpeg":"image/jpeg", "jpg":"image/jpeg", "js":"application/x-javascript", "kar":"audio/midi", "latex":"application/x-latex", "lha":"application/octet-stream", "lzh":"application/octet-stream", "m3u":"audio/x-mpegurl", "m4a":"audio/mp4a-latm", "m4b":"audio/mp4a-latm", "m4p":"audio/mp4a-latm", "m4u":"video/vnd.mpegurl", "m4v":"video/x-m4v", "mac":"image/x-macpaint", "man":"application/x-troff-man", "mathml":"application/mathml+xml", "me":"application/x-troff-me", "mesh":"model/mesh", "mid":"audio/midi", "midi":"audio/midi", "mif":"application/vnd.mif", "mov":"video/quicktime", "movie":"video/x-sgi-movie", "mp2":"audio/mpeg", "mp3":"audio/mpeg", "mp4":"video/mp4", "mpe":"video/mpeg", "mpeg":"video/mpeg", "mpg":"video/mpeg", "mpga":"audio/mpeg", "ms":"application/x-troff-ms", "msh":"model/mesh", "mxu":"video/vnd.mpegurl", "nc":"application/x-netcdf", "oda":"application/oda", "ogg":"application/ogg", "pbm":"image/x-portable-bitmap", "pct":"image/pict", "pdb":"chemical/x-pdb", "pdf":"application/pdf", "pgm":"image/x-portable-graymap", "pgn":"application/x-chess-pgn", "pic":"image/pict", "pict":"image/pict", "png":"image/png", "pnm":"image/x-portable-anymap", "pnt":"image/x-macpaint", "pntg":"image/x-macpaint", "ppm":"image/x-portable-pixmap", "ppt":"application/vnd.ms-powerpoint", "ps":"application/postscript", "qt":"video/quicktime", "qti":"image/x-quicktime", "qtif":"image/x-quicktime", "ra":"audio/x-pn-realaudio", "ram":"audio/x-pn-realaudio", "ras":"image/x-cmu-raster", "rdf":"application/rdf+xml", "rgb":"image/x-rgb", "rm":"application/vnd.rn-realmedia", "roff":"application/x-troff", "rtf":"text/rtf", "rtx":"text/richtext", "sgm":"text/sgml", "sgml":"text/sgml", "sh":"application/x-sh", "shar":"application/x-shar", "silo":"model/mesh", "sit":"application/x-stuffit", "skd":"application/x-koan", "skm":"application/x-koan", "skp":"application/x-koan", "skt":"application/x-koan", "smi":"application/smil", "smil":"application/smil", "snd":"audio/basic", "so":"application/octet-stream", "spl":"application/x-futuresplash", "src":"application/x-wais-source", "sv4cpio":"application/x-sv4cpio", "sv4crc":"application/x-sv4crc", "svg":"image/svg+xml", "swf":"application/x-shockwave-flash", "t":"application/x-troff", "tar":"application/x-tar", "tcl":"application/x-tcl", "tex":"application/x-tex", "texi":"application/x-texinfo", "texinfo":"application/x-texinfo", "tif":"image/tiff", "tiff":"image/tiff", "tr":"application/x-troff", "tsv":"text/tab-separated-values", "txt":"text/plain", "ustar":"application/x-ustar", "vcd":"application/x-cdlink", "vrml":"model/vrml", "vxml":"application/voicexml+xml", "wav":"audio/x-wav", "wbmp":"image/vnd.wap.wbmp", "wbmxl":"application/vnd.wap.wbxml", "wml":"text/vnd.wap.wml", "wmlc":"application/vnd.wap.wmlc", "wmls":"text/vnd.wap.wmlscript", "wmlsc":"application/vnd.wap.wmlscriptc", "wrl":"model/vrml", "xbm":"image/x-xbitmap", "xht":"application/xhtml+xml", "xhtml":"application/xhtml+xml", "xls":"application/vnd.ms-excel", "xml":"application/xml", "xpm":"image/x-xpixmap", "xsl":"application/xml", "xslt":"application/xslt+xml", "xul":"application/vnd.mozilla.xul+xml", "xwd":"image/x-xwindowdump", "xyz":"chemical/x-xyz", "zip":"application/zip"}

def get_mime(name):
    ext = name.rsplit('.',1)[1]
    return mime_types.get(ext,"text/html")
def part_sum(data):
    return (sum([ord(c) for c in data[0::2]])<<8) + sum([ord(c) for c in data[1::2]])

def str2hex(data):
    return (reduce(lambda a,b: a+","+hex(ord(b)),data,""), len(data), part_sum(data))

def file2hex(filename):
    f = file(filename,"rb")
    data = f.read();
    mime = get_mime(filename)
    print mime
    data = header_ok%(len(data),mime)+data
    return str2hex(data)
#    return (reduce(lambda a,b: a+","+hex(ord(b)),data,""), len(data), part_sum(data))

def get_names(path):
    return reduce(lambda a,b: a+get_names(path+"/"+b),os.listdir(path),[]) if os.path.isdir(path) else [path]

def build_fs_old(path):
    filenames = get_names(path);
    sys_info = ""
    data = ""
    offset=0;
    for f in filenames:
        fb, count = file2hex(f)
        data+=fb
        sys_info+=', {"'+f[len(path):]+'",'+hex(offset)+","+hex(count)+",0}"
        offset+=count;
    sys_info+= ', {"",0,0,0}'
    return 'const romfs_entry files[] __attribute__ ((section (".store"))) = {' + sys_info[2:]+'};\nconst unsigned char filedata[] __attribute__ ((section (".store"))) = {'+data[1:]+"};\n"

def build_fs(path):
    files = [(f[len(path):], file2hex(f)) for f in get_names(path)]
    files += [('',str2hex(all_404))]
    data=''
    sys_info=''
    offset=0
    for name, (filedata, count, psum) in files:
        sys_info+=', {"%s",0x%x,0x%x,0x%x}'%(name,offset,count,psum)
        data+=filedata
        offset+=count
    of = file('romfs.cpp','w')
    of.write('#include "romfs.h"\n')
    of.write('const romfs_entry files[] __attribute__ ((section (".store"))) = {')
    of.write(sys_info[2:])
    of.write('};\nconst unsigned char filedata[] __attribute__ ((section (".store"))) = {')
    of.write(data[1:])
    of.write("};\n")

#    return 'const romfs_entry files[] __attribute__ ((section (".store"))) = {' + sys_info[2:]+'};\nconst unsigned char filedata[] __attribute__ ((section (".store"))) = {'+data[1:]+"};\n"
