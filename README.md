TCP/IP stack for Bare-metal systems.

Full custom implementation of TCP/IP stack in C++.
Has built-in memory pools for all structures, so no dynamic allocation ever happens.
Built for Atmel AT91SAMX series of MCUs, but can be modified to run on other ARM devices and requires no OS.
The project contains a simple web server with pregenerated RO filesystem and python scripts for generating it.
It includes a simple pin value change counter and a Java Applet (don't kill me, seems like I wrote this a lifetime ago) that connects to it.

The stack itself is highly optimized for the MCU and serves the files with zero copying using DMA and the gather functionality of the built-in MAC.

Why and What?

This is something I wrote for a commercial project of mine a long time ago. The project is no longer amongst the living, so now it's open-source.
Can be built using CodeSourcery ARM toolchain lite.
